﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    abstract class SearchStrategy
    {
        public abstract bool Search(double[] array,double searchNumber);

    }
}
