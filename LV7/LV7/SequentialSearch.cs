﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    class SequentialSearch:SearchStrategy
    {
        public override bool Search(double[] array,double searchNumber)
        {
            for(int i=0;i<array.Length;i++)
            {
                if (array[i] == searchNumber)
                    return true;

            }

            return false;
        }
    }
}
