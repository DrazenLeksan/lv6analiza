﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4
{
    class EmailValidator
    {
        public bool isValidEmail(String candidate)
        {
            if(String.IsNullOrEmpty(candidate))
            {
                return false;
            }

            return ContainsAt(candidate) && ContainsComOrHr(candidate);
        }

        private bool ContainsAt(String candidate)
        {
            if (candidate.Contains("@"))
            {
                return true;
            }

            else
                return false;
        }

        private bool ContainsComOrHr(string candidate)
        {
            if (candidate.Contains(".com") || candidate.Contains(".hr"))
            {
                return true;
            }

            else
                return false;
        }


          
    }
}
