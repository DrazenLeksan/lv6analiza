﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4
{
    class DiscountedItem : RentableDecorator
    {
        private double discountPercent = 10;
        public double DiscountPercentđ
        {
            get => discountPercent;
            set => discountPercent = value;
        }
            

        public DiscountedItem(IRentable rentable) : base(rentable) { }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() -discountPercent/100*base.CalculatePrice();
        }
        public override String Description
        {
            get
            {
                return "now at "+this.discountPercent+"% of " + base.Description;
            }
        }
    }
}
