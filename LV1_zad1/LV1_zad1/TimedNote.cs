﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1_zad1
{
    class TimedNote: Note
    {
        private DateTime time;

        public TimedNote(string text, string author, int priority, DateTime time)
        :base(text,author,priority)
        {
            this.time = time;
        }

        public override string ToString()
        {
            return Text + ", " + Author + " " + this.time;
        }

        public string MyToString()
        {
            return Text + ", " + Author + " " + this.time;
        }



    }
}
