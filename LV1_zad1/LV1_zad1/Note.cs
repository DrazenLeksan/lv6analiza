﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1_zad1
{
    class Note
    {
        private string text;
        private string author;
        private int priority;

        public Note()
        {
            this.text = "blank note";
            this.author = "Anonymous";
            this.priority = 0;
        }

        public Note(string text, int priority)
        {
            this.text = text;
            this.author = "Anonymous";
            this.priority = priority;

        }

        public Note(string text, string author, int priority)
        {
            this.text = text;
            this.author = author;
            this.priority = priority;
        }
        public string getText()
        {
            return this.text;
        }

        public void setText(string text)
        {
            this.text = text;
        }

        public string getAuthor()
        {
            return this.author;
        }

        public void setPriority(int priority)
        {
            this.priority = priority;
        }

        public int getPriority()
        {
            return this.priority;
        }

        public string Text
        {
            get { return this.text; }
             
            set { this.text = value; }
        }

        public string Author
        {
            get { return this.author; }

            set { this.author = value; }
        }

        public int Priority
        {
            get { return this.Priority; }

            set { this.Priority = value; }
        }

        public override string ToString()
        {
            return this.text + ", " + this.author + "(" + this.priority + ")";
        }
    }
}
