﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace LV1_zad1
{
    class TasksToDo:Note
    {
        private List<Note> Tasks = new List<Note>();
   
        public void AddTask(Note task)
        {
            
            Tasks.Add(task);
        }

        public void RemoveTask(Note task)
        {
            Tasks.Remove(task);
        }

        public int getHighestPriority()
        {
            int[] priorities = new int[Tasks.Count];
            int i = 0;

            foreach (Note obj in Tasks)
            {
                priorities[i] = obj.getPriority();
                i++;
            }

            return  priorities.Min();

        }

        public void PerformHighestPriorityNotes()
        {
            foreach (Note obj in Tasks.ToList())
            {
                if (obj.getPriority() == getHighestPriority())
                {
                    RemoveTask(obj);
                }
            }
        }

        public List<Note> getTasks()
        {
            return Tasks;
        }

    }
}
