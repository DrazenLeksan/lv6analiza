﻿using System;
using System.Linq;

namespace LV1_zad1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Note blank = new Note();
            Note assigmentUntill = new Note("Deadline is 01/04/2020", "John", 1);
            Note lectureTime = new Note("Lecture tomorrow at 8", 2);
            TimedNote timedAssigmentUntill = new TimedNote("Deadline is 01/04/2020", "John", 1, DateTime.Now);
            

            Console.WriteLine(blank.getAuthor());
            Console.WriteLine(blank.getText());

            Console.WriteLine(assigmentUntill.getAuthor());
            Console.WriteLine(assigmentUntill.getText());

            Console.WriteLine(lectureTime.getAuthor());
            Console.WriteLine(lectureTime.getText());

            Console.WriteLine(timedAssigmentUntill.ToString());

            string text, author;
            int priority;
            TasksToDo tasks = new TasksToDo(); 

            for(int i=0;i<3;i++)
            {
                Console.WriteLine("Enter the text of a note:");
                text = Console.ReadLine();

                Console.WriteLine("Enter the author of a note:");
                author = Console.ReadLine();

                Console.WriteLine("Enter the priority of a note:");
                priority = Convert.ToInt32(Console.ReadLine());
                 
                tasks.AddTask(new Note(text, author, priority));

            }
            

            foreach(Note obj in tasks.getTasks())
            {
                Console.WriteLine(obj.ToString());
            }


            tasks.PerformHighestPriorityNotes();


            foreach (Note obj in tasks.getTasks())
            {
                Console.WriteLine(obj.ToString());
            }



        }
    }

}

