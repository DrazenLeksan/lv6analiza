﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LV7zad3_4
{
    class SystemDataProvider:SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad - this.previousCPULoad >= this.previousCPULoad * 0.1 || this.previousCPULoad - currentLoad >= 0.1 * this.previousCPULoad)
            {
                this.Notify();
            }

            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentAvailableRAM = this.AvailableRAM;
            if (currentAvailableRAM - this.previousRAMAvailable >= this.previousRAMAvailable * 0.1 || this.previousRAMAvailable - currentAvailableRAM >= 0.1 * this.previousRAMAvailable)
            {
                this.Notify();
            }

            this.previousRAMAvailable = currentAvailableRAM;
            return currentAvailableRAM;
        }
    }
}
