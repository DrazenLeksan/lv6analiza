﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7zad3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider provider = new SystemDataProvider();
            ConsoleLogger logger = new ConsoleLogger();
            while(true)
            {
                logger.Log(provider);
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
