﻿using System;
//Kreirajte objekt klase DiceRoller i u njega ubacite 20 kockica. Baciti sve kockice i ispisati rezultate bacanja
//kockica na ekran.
namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. zadatak
            //DiceRoller RollingDice=new DiceRoller();
            //for (int i = 0; i < 20; i++) 
            //{
            //    RollingDice.InsertDie(new Die(6));
            //}

            //RollingDice.RollAllDice();

            //RollingDice.PrintResults();


            //2. zadatak
            //DiceRoller RollingDice = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    RollingDice.InsertDie(new Die(6,new Random()));
            //}

            //RollingDice.RollAllDice();

            //RollingDice.PrintResults();


            //3. zadatak
            //DiceRoller RollingDice = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    RollingDice.InsertDie(new Die(6));
            //}

            //RollingDice.RollAllDice();

            //RollingDice.PrintResults();


           // 4.zadatak
            DiceRoller RollingDiceConsole = new DiceRoller();
            DiceRoller RollingDiceFile = new DiceRoller("D:\\Program Files\\log.txt");
            for (int i = 0; i < 20; i++)
            {
                RollingDiceConsole.InsertDie(new Die(6));
                RollingDiceFile.InsertDie(new Die(6));

            }


            RollingDiceFile.RollAllDice();
            RollingDiceFile.LogRollingResults();
            RollingDiceConsole.RollAllDice();
            RollingDiceConsole.LogRollingResults();






        }
    }
}
