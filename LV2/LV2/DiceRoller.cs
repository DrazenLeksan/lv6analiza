﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2
{
    class DiceRoller
    {
        //1.,2.,3. zadatak
        //private List<Die> dice;
        //private List<int> resultForEachRoll;
        //public DiceRoller()
        //{
        //    this.dice = new List<Die>();
        //    this.resultForEachRoll = new List<int>();
        //}
        //public void InsertDie(Die die)
        //{
        //    dice.Add(die);
        //}
        //public void RollAllDice()
        //{
        //    //clear results of previous rolling
        //    this.resultForEachRoll.Clear();
        //    foreach (Die die in dice)
        //    {
        //        this.resultForEachRoll.Add(die.Roll());
        //    }
        //}
        ////View of the results
        //public IList<int> GetRollingResults()
        //{
        //    return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
        //   this.resultForEachRoll
        //   );
        //}

        //public void PrintResults()
        //{
        //    foreach(int result in resultForEachRoll)
        //    {
        //        Console.WriteLine(result);
        //    }
        //}
        //public int DiceCount
        //{
        //    get { return dice.Count; }
        //}



        //4. zadatak
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger logger;
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            logger = new ConsoleLogger();
        }

        public DiceRoller(string filePath)
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.logger = new FileLogger(filePath);
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            //clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }

        public void PrintResults()
        {
            foreach (int result in resultForEachRoll)
            {
                Console.WriteLine(result);
            }
        }

        public void LogRollingResults()
        {
            foreach (int result in this.resultForEachRoll)
            {
                logger.Log(result.ToString());
            }

        }

        public int DiceCount
        {
            get { return dice.Count; }
        }






    }
}
