﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class RandomMatrixGenerator
    {
        private static RandomMatrixGenerator instance;
             
        public static RandomMatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomMatrixGenerator();
            }
            return instance;
        }
        
        //Navedena metoda ima 2 odgovornosti alociranje memorije za matricu i njeno popunjavanje
        public double[][] GenerateMatrix(int rowCount,int columnCount)
        {
            double[][] matrix = new double[rowCount][];
            for (int i=0;i<rowCount;i++)
            {
                matrix[i] = new double[columnCount];
            }

            for(int i=0;i<rowCount;i++)
            {
                for(int j=0;j<columnCount;j++)
                {
                    matrix[i][j] = RandomGenerator.GetInstance().NextDouble();
                }
            }

            return matrix;
        }
         

        

    }
}
