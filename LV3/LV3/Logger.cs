﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace LV3
{
    class Logger
    {
        private static Logger instance;
        public string Path { get; set; }
        

        public static Logger GetInstance()
        {
            if(instance==null)
            {
                instance = new Logger();
            }

            return instance;
        }
        Logger()
        {
            this.Path = "D:\\Program Files\\log";
        }

        Logger(string path)
        {
            this.Path = path;
        }

        

        //Uporaba loggera ce pisati u istu datoteku u drugim mjestima u programu
        public void Log(string message)
        {
            using (StreamWriter streamWriter = new StreamWriter(this.Path))
            {
                streamWriter.WriteLine(message);
            }

        }



    }
}
