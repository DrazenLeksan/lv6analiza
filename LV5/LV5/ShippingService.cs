﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5
{
    class ShippingService
    {
        private double pricePerKg;

        public  ShippingService(double pricePerKg)
        {
            this.pricePerKg = pricePerKg;
        }
        public double Price(double weight)
        {
            return pricePerKg * weight;
        }

    }
}
